class Post < ActiveRecord::Base
  belongs_to :post_category
  belongs_to :user
  
  has_many :comments

  validates_presence_of :post_category_id, :title, :desc
end
